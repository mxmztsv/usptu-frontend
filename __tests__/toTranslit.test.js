import {toTranslit} from "../src/services/translitService";

test('Транслитерация: одно слово', () => {
	expect(toTranslit('проверка')).toBe('proverka')
})

test('Транслитерация: с пробелом', () => {
	expect(toTranslit('проверка с пробелом')).toBe('proverka_s_probelom')
})
