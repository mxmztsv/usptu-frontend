resource "sbercloud_vpc" "devops-lab" {
  name = "devops-lab"
  cidr = "192.168.0.0/16"
}

resource "sbercloud_vpc_subnet" "subnet_devops-lab" {

  name = "subnet_devops-lab"

  cidr       = "192.168.0.0/24"
  gateway_ip = "192.168.0.1"

  primary_dns   = "100.125.13.59"
  secondary_dns = "1.1.1.1"

  vpc_id = sbercloud_vpc.devops-lab.id

}

resource "sbercloud_vpc_eip" "eip_devops-lab" {

  publicip {
    type = "5_bgp"
  }

  bandwidth {
    name        = "elb_bandwidth"
    size        = 100
    share_type  = "PER"
    charge_mode = "bandwidth"
  }

}

resource "sbercloud_networking_secgroup" "secgroup_devops-lab" {
  name        = "secgroup_devops-lab"
  description = "bla bla"
}

resource "sbercloud_networking_secgroup_rule" "secgroup_devops-lab_rule_22" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = sbercloud_networking_secgroup.secgroup_devops-lab.id
}

resource "sbercloud_networking_secgroup_rule" "secgroup_devops-lab_rule_80" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = sbercloud_networking_secgroup.secgroup_devops-lab.id
}

resource "sbercloud_networking_secgroup_rule" "secgroup_devops-lab_rule_443" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = sbercloud_networking_secgroup.secgroup_devops-lab.id
}

resource "sbercloud_networking_secgroup_rule" "secgroup_devops-lab_rule_5432" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5432
  port_range_max    = 5432
  remote_ip_prefix  = "192.168.0.0/16"
  security_group_id = sbercloud_networking_secgroup.secgroup_devops-lab.id
}

data "sbercloud_images_image" "ubuntu_image" {
  name        = "Ubuntu 22.04 server 64bit"
  most_recent = true
}

resource "sbercloud_compute_keypair" "devops-lab" {
  name       = "terraform-keypair"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCxDr88oJ7hZtCseQ4kNV15o8gmdWIdXGRKpHFqIvLTPAF9r/VLvW5LG+LW9GmfPnDRp+gt39l3HZqG3NOTdHqrEP3cIsFz5yiPuMcahjUQwhRelZTzlT7Bc09uQNUUrasAbdLZtq/iRUvAJEak0amUV7iW4qm9XiQPnAor30nJx3M/yG2emGbgFnmnLS/3ZmoodErzn3cSxg3M1R6BaQkf9nN2Tuzg5mWvG3k5zvl33r+0f7xOMUKQG3WkfkaSFABlBCfsVynCVvD8vNpnnmYyLC/ddwAFUQD4zL+uiz2vsfHp2+Mz2wFg0FjKokdHICYq1g10EReDmO8nTjlChHohzGAon9E1lA/hc7JsISUs8G+8PXAItWi1BFRVM3ynnwSEOJsGoAwtcrSdMRw7sw78/9skdZF40IBl0PwTA1bmgFxp7jZZF6HysBYvK6b91c/gzJYDGGw2hh5995FjhE9TEvDVoJZSOu974DnigG6uaGicKkmLNB2XTfuDItu29rs= maxim@Maxim_PC"
}

resource "sbercloud_compute_instance" "ecs_devops-lab" {
  name              = "ecs-devops-lab"
  image_id          = data.sbercloud_images_image.ubuntu_image.id
  flavor_id         = "s6.small.1"
  security_groups   = ["default", sbercloud_networking_secgroup.secgroup_devops-lab.name]
  availability_zone = "ru-moscow-1b"
  key_pair          = sbercloud_compute_keypair.devops-lab.name
  system_disk_type = "SAS"
  system_disk_size = 20

  network {
    uuid = sbercloud_vpc_subnet.subnet_devops-lab.id
  }

}

resource "sbercloud_compute_eip_associate" "associated_eip_devops-lab" {
  public_ip   = sbercloud_vpc_eip.eip_devops-lab.address
  instance_id = sbercloud_compute_instance.ecs_devops-lab.id
  fixed_ip    = sbercloud_compute_instance.ecs_devops-lab.network.0.fixed_ip_v4
}
