export const PORT = '4000' // Порт, на котором запущен бэкенд-сервер
export const BASE_S3_URL = `http://storage.yandexcloud.net/devops-itmo-bucket` // Базовый url s3
export const GENERATED_DOCS_URL = `${BASE_S3_URL}/generated-docs`
export const UPLOADED_DOCS_URL = `${BASE_S3_URL}/uploaded-docs`
// https://storage.yandexcloud.net/devops-itmo-bucket/docs-templates/internship_form_template.docx
// export const BASE_URL = `http://localhost:${PORT}` // Базовый url бэкенда
export const BASE_URL = `http://usptu-api.maximzaytsev.ru` // Базовый url бэкенда
